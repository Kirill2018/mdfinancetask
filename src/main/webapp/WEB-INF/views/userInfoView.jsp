    <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>User Info</title>
</head>
<body>

<jsp:include page="menu.jsp"></jsp:include>

<h3>Hello: ${loginedUser.userName}</h3>

User Name: <b>${loginedUser.userName}</b>
<br/>
Gender: <b>${loginedUser.gender }</b>
<br/>
Password: <b>${loginedUser.password }</b>
<br/>
<p style="color: red;">${errorMessage}</p>
<form method="POST" action="${pageContext.request.contextPath}/user">
    <input type="hidden" name="id" value="${loginedUser.id}"/>
    <table border="0">
        <tr>
            <td>User Name</td>
            <td><input type="text" name="userName" value="${user.userName}"/></td>
        </tr>
        <tr>
            <td>Gender</td>
            <td><input type="text" name="gender" value="${user.gender}"/></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" value="${user.password}"/></td>
        </tr>

        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"/>
                <a href="${pageContext.request.contextPath}/">Cancel</a>
            </td>
        </tr>
    </table>
</form>


</body>
</html>