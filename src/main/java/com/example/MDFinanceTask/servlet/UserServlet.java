package com.example.MDFinanceTask.servlet;

import com.example.MDFinanceTask.dto.User;
import com.example.MDFinanceTask.util.AppUtils;
import com.example.MDFinanceTask.util.db.dao.UserDAO;
import com.example.MDFinanceTask.util.validate.UserValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Page with user info
 */

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UserServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher //
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");

        dispatcher.forward(request, response);
    }

    /**
     * Validate and write updated user info
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserValidator validator = new UserValidator();
        UserDAO userDAO = new UserDAO();
        try {
            User user = validator.validate(request);
            userDAO.update(user);
            AppUtils.storeLoginedUser(request.getSession(), user);
        } catch (Exception ex) {
            request.setAttribute("errorMessage", ex.getMessage());
        } finally {
            doGet(request, response);
        }


    }
}