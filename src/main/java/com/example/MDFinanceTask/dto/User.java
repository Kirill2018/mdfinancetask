package com.example.MDFinanceTask.dto;

/**
 * User DTO which contains info from DB about user
 */

public class User implements DTO {

    private Long id;
    private String userName;
    private String gender;
    private String password;

    public User() {
    }

    public User(Long id, String userName, String password, String gender) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.gender = gender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
