package com.example.MDFinanceTask.util.db.dao;

import com.example.MDFinanceTask.dto.DTO;

import java.sql.SQLException;

/**
 * Interface fot Data Access Objects which have managed DB info
 *
 * @param <T> - Concrete DTO for data management
 */
public interface DAO<T extends DTO> {
    T find(String username, String password) throws SQLException;

    boolean update(T dto) throws SQLException;
}
