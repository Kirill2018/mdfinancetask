package com.example.MDFinanceTask.util.db.dao;

import com.example.MDFinanceTask.util.db.ConnectionFactory;
import com.example.MDFinanceTask.dto.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DAO for users table
 */
public class UserDAO implements DAO<User> {

    /**
     * Find a User by userName and password.
     *
     * @param userName
     * @param password
     * @return finded user or null
     * @throws SQLException
     */
    public User find(String userName, String password) throws SQLException {

        ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();

        PreparedStatement getUserByParams = connectionFactory.getConnection().prepareStatement(
                "select u.id, u.gender from Users u where u.username = ? and u.password = ?");

        getUserByParams.setString(1, userName);
        getUserByParams.setString(2, password);

        ResultSet resultSet = getUserByParams.executeQuery();

        User user = null;
        if (resultSet.next()) {
            user = new User(resultSet.getLong(1), userName, password, resultSet.getString(2));
        }

        return user;
    }

    /**
     * Update all info for concrete record
     *
     * @param dto - user info
     * @return true if all was right
     * @throws SQLException
     */
    @Override
    public boolean update(User dto) throws SQLException {
        ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();

        PreparedStatement updateUser = connectionFactory.getConnection().prepareStatement(
                "update users set username = ?, gender = ?, password = ? where id = ?");

        updateUser.setString(1, dto.getUserName());
        updateUser.setString(2, dto.getGender());
        updateUser.setString(3, dto.getPassword());
        updateUser.setLong(4, dto.getId());

        int i = updateUser.executeUpdate();

        if (i == 1) {
            return true;
        }

        return false;
    }
}
