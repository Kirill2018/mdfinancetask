package com.example.MDFinanceTask.util.validate;

import com.example.MDFinanceTask.dto.DTO;

import javax.servlet.http.HttpServletRequest;

/**
 * Interface of validators DTO data
 */
public interface DTOParametersValidator {
    DTO validate(HttpServletRequest requestParameters);
}
