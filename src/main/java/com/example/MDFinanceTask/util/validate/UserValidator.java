package com.example.MDFinanceTask.util.validate;

import com.example.MDFinanceTask.dto.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Locale;

/**
 * Validate user DTO data
 */
public class UserValidator implements DTOParametersValidator {

    /**
     * Validate each user parameter with help enum
     *
     * @param requestParameters
     * @return user with correct parameters
     * @throws IllegalArgumentException if one from parameters is invalid
     */
    @Override
    public User validate(HttpServletRequest requestParameters) {
        User user = new User();
        for (UserParameters userParameter : UserParameters.values()) {
            String requestParameterValue = requestParameters.getParameter(userParameter.parameterName);
            if (!userParameter.validate(requestParameterValue, user)) {
                throw new IllegalArgumentException("Parameter " + userParameter.parameterName + " is incorrect");
            }
        }

        return user;
    }

    private enum UserParameters {
        ID("id") {
            @Override
            public boolean validate(String value, User user) {
                if (defaultCheck(value)) {
                    user.setId(Long.parseLong(value));
                    return true;
                }

                return false;
            }
        },
        USERNAME("userName") {
            @Override
            public boolean validate(String value, User user) {
                if (defaultCheck(value)) {
                    user.setUserName(value);
                    return true;
                }

                return false;
            }
        },
        GENDER("gender") {
            @Override
            public boolean validate(String value, User user) {
                final String[] genders = {"M", "F"};
                if (value != null &&
                        Arrays.stream(genders).anyMatch(gender -> value.toUpperCase(Locale.ROOT).equals(gender))) {
                    user.setGender(value);
                    return true;
                }

                return false;
            }
        },
        PASSWORD("password") {
            @Override
            public boolean validate(String value, User user) {
                if (defaultCheck(value)) {
                    user.setPassword(value);
                    return true;
                }

                return false;
            }
        };

        /**
         * Validate and write correct parameters
         *
         * @param value
         * @param user
         * @return
         */
        public abstract boolean validate(String value, User user);

        protected boolean defaultCheck(String value) {
            return value != null && value.length() > 0;
        }

        public String parameterName;

        UserParameters(String parameterName) {
            this.parameterName = parameterName;
        }
    }
}
