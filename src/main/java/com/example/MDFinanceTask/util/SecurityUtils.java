package com.example.MDFinanceTask.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Manage security pages
 */
public class SecurityUtils {

    private static final String securityPage = "/user";

    /**
     * Check needing of this request fot entering into system
     *
     * @param request
     * @return
     */
    public static boolean isSecurityPage(HttpServletRequest request) {
        String urlPattern = URLPatternUtils.getUrlPattern(request);

        return securityPage.equals(urlPattern);
    }
}
